#include "GraphNode.h"
#include <QPen>
#include <QGraphicsTextItem>
#include <QFont>

GraphNode::GraphNode(qreal x, qreal y, qreal yUnit, int value, QGraphicsItem *parent) :
    QGraphicsEllipseItem(x-3,y-3,6,6,parent)
  , m_value(value)
  , m_text(this)
  , m_line(x, y, x, y+yUnit*value,this)
{
    setPen(QPen(QBrush(QColor(Qt::blue)),2));
    setBrush(QBrush(QColor(Qt::white)));
    m_text.setPos(x-3-10, y-3-26);
    QFont font = m_text.font();
    font.setPointSize(16);
    m_text.setFont(font);
    m_text.setVisible(false);
    m_text.setHtml("<span style='background-color: blue; color: white;'>" +
                   QString::number(value) + "</span>" );

    setAcceptHoverEvents(true);
    m_line.setPen(QPen(QBrush(QColor(Qt::darkCyan)), 1, Qt::DashLine));
}

void GraphNode::setValue(int value) {
    m_value = value;
}

int GraphNode::getValue() {
    return m_value;
}

void GraphNode::hoverEnterEvent(QGraphicsSceneHoverEvent *){
    m_text.setVisible(true);
}

void GraphNode::hoverLeaveEvent(QGraphicsSceneHoverEvent *){
    m_text.setVisible(false);
}

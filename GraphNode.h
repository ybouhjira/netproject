#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include <QGraphicsEllipseItem>
class QGraphicsTextItem;
class QGraphicsLineItem;

class GraphNode : public QGraphicsEllipseItem
{
public:
    explicit GraphNode(qreal x, qreal y, qreal yUnit,int value=0, QGraphicsItem *parent = 0);

    void setValue(int value);

    int getValue();

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

private:
    int m_value ;
    QGraphicsTextItem m_text;
    QGraphicsLineItem m_line;

};

#endif // GRAPHNODE_H

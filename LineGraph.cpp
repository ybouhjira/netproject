#include "LineGraph.h"
#include "GraphNode.h"

#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QVector>
#include <QGraphicsTextItem>


LineGraph::LineGraph
(qreal xUnit, qreal yUnit, QObject *parent)
  : QGraphicsScene(parent)
  , m_xUnit(xUnit)
  , m_yUnit(yUnit)
  , m_axisPen(QBrush(QColor(Qt::black)),2)
  , m_graphPen(QBrush(QColor(20,120,220)),2)
  , m_path(new QGraphicsPathItem)
{
    addItem(m_path);
    m_path->setPen(m_graphPen);
    addLine(0,0,xUnit*101,0,m_axisPen);
    addLine(0,0,0,-yUnit*37,m_axisPen);

    for(qreal i=1; i<=100; i++){
        if((int)i % 5 == 0){
            QGraphicsTextItem *item = addText(QString::number(i));
            item->setPos(i*xUnit-4, 0);
            addEllipse(i*xUnit-2, 0, 4, 4, QPen(), QBrush(QColor(Qt::black)) );
        } else {
            addEllipse(i*xUnit-2, 0, 2, 2, QPen(), QBrush(QColor(Qt::black)) );
        }

    }

    for(qreal i=0; i<37; i++){
        if((int)i % 5 == 0){
            QGraphicsTextItem *item = addText(QString::number(i));
            item->setPos(-24, -i*yUnit-12);
            addEllipse(0, -i*yUnit-2, 4, 4, QPen(), QBrush(QColor(Qt::black)) );
        } else {
            addEllipse(0, -i*yUnit-2, 2, 2, QPen(), QBrush(QColor(Qt::black)) );
        }

    }

}

void LineGraph::draw(QVector<int> const &data) {
    QPainterPath path;

    foreach(GraphNode* node, m_nodes)
        delete node;
    m_nodes.clear();

    // draw first point
    path.moveTo(m_xUnit, -data[0]*m_yUnit );

    for(int x=0, size=data.size(); x<size; x++){
        qreal xi = (x+1) * m_xUnit ;
        qreal yi = - data[x] * m_yUnit;
        path.lineTo(xi, yi);
        m_nodes.push_back(new GraphNode(xi, yi, m_yUnit, data[x]));
        addItem(m_nodes.last());
    }
    m_path->setPath(path);
}

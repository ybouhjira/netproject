#ifndef LINEGRAPH_H
#define LINEGRAPH_H

#include <QGraphicsScene>
class GraphNode;

class QGraphicsPathItem;

class LineGraph : public QGraphicsScene
{
    Q_OBJECT
public:
    LineGraph( qreal xUnit, qreal yUnit
              ,QObject *parent = 0);

signals:

public slots:
    void draw(QVector<int> const& data);

private:
    qreal m_xUnit, m_yUnit;
    QPen m_axisPen, m_graphPen;

    QGraphicsPathItem *m_path;
    QVector<GraphNode*> m_nodes;
};

#endif // LINEGRAPH_H

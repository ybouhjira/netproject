#include "MainWindow.h"
#include "LineGraph.h"
#include "ResultModel.h"

// Qt includes
#include <QTableView>
#include <QHeaderView>
#include <QSplitter>
#include <QAction>
#include <QToolBar>
#include <QGraphicsView>
#include <QTableWidget>
#include <QDockWidget>
#include <QMenu>
#include <QMenuBar>
#include <QFileDialog>
#include <QMessageBox>
#include <QSvgGenerator>
#include <QPdfWriter>
#include <QScopedPointer>
#include <QRegExp>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_graphMenu(new QMenu("Graph"))
    , m_matrixMenu(new QMenu("Matrice"))
    , m_reloadAction(new QAction(QIcon(":/ico/reload"),"Nouvelle matrice",this))
    , m_saveAction(new QAction(QIcon(":/ico/save"),"Enregistrer",this))
    , m_randMatrixView(new QTableView)
    , m_trameView(new QTableView)
    , m_randMatrixModel(new ResultModel(RandomMatrix(100,36),this))
    , m_trameModel(new MatrixModel(RandomMatrix(1,36),this))
    , m_graphicsView(new QGraphicsView)
    , m_lineGraph(new LineGraph(20,10))
    , m_matrixDock(new QDockWidget("Matrice"))
    , m_trameDock(new QDockWidget("Trame"))
{

    // GRAPH -------------------------------------------------------------------
    setCentralWidget(m_graphicsView);
    m_graphicsView->setScene(m_lineGraph);
    m_graphicsView->setRenderHint(QPainter::Antialiasing,true);

    // DOCKS -------------------------------------------------------------------
    addDockWidget(Qt::LeftDockWidgetArea, m_matrixDock);
    addDockWidget(Qt::BottomDockWidgetArea, m_trameDock);
    tabifyDockWidget(m_trameDock, m_matrixDock);

    m_matrixDock->setWidget(m_randMatrixView);
    m_randMatrixView->setModel(m_randMatrixModel);
    m_randMatrixView->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    m_randMatrixView->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
    m_randMatrixView->setEditTriggers(QTableView::NoEditTriggers);
    m_randMatrixView->setSelectionMode(QAbstractItemView::NoSelection);
    m_trameDock->setWidget(m_trameView);
    m_trameView->setModel(m_trameModel);
    m_trameView->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    m_trameView->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);

    updateGraph();

    // toolBar
    QToolBar *toolbar = new QToolBar("Barre d'outils");
    addToolBar(toolbar);
    toolbar->addAction(m_reloadAction);
    toolbar->addAction(m_saveAction);
    toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    // MENUS -------------------------------------------------------------------
    setMenuBar(new QMenuBar);
    menuBar()->addMenu(m_graphMenu);
    menuBar()->addMenu(createPopupMenu())->setText("Affichage");
    menuBar()->addMenu(m_matrixMenu);
    m_graphMenu->addAction(m_saveAction);
    m_matrixMenu->addAction(m_reloadAction);

    // CONNECTIONS -------------------------------------------------------------
    // Menu graph
    connect(m_saveAction, SIGNAL(triggered()), this, SLOT(saveGraph()) );
    // Menu matrice
    connect(m_reloadAction, SIGNAL(triggered()), this, SLOT(reloadMatrix()));
    // QTableView
    connect(m_randMatrixModel, SIGNAL(modelReset()), this, SLOT(updateGraph()));
    connect(m_trameModel, SIGNAL(modelReset()), this, SLOT(updateGraph()) );
    connect(m_trameModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this,
            SLOT(updateGraph()));

}

void MainWindow::reloadMatrix(){
    m_randMatrixModel->reloadMatrix(100,36);
}

void MainWindow::updateGraph() {
    QVector<int> graphData = calculate();
    m_lineGraph->draw(graphData);
}

QVector<int> MainWindow::calculate() {
    std::vector<char> trame = m_trameModel->getMatrix()(0);
    RandomMatrix matrix = m_randMatrixModel->getMatrix();
    QVector<int> result(100);

    for(int i=0, count=0; i<100; i++, count = 0) {
        for(int j=0; j<36; j++) {
            if((int)trame[j] == matrix(i ,j)) {
                count++ ;
                m_randMatrixModel->setMatch(i ,j ,true);
            }
        }
        result[i] = count;
    }
    return result;
}

void MainWindow::saveGraph(){
    QString selectedFormat;
    QString fileName = QFileDialog::getSaveFileName(
                this,
                "Enregistrer le Graph",
                QDir::home().absolutePath(),
                "PDF(*.pdf);;SVG(*.svg)",
                &selectedFormat);

    QScopedPointer<QPaintDevice> device;
    QSizeF sizeF = m_lineGraph->itemsBoundingRect().size();

    if(!fileName.isEmpty()) {
        // Adding the extension to the fileName
        QString extension = ((selectedFormat=="PDF(*.pdf)")? "pdf":"svg") ;
        QRegExp regexp("*." + extension);
        regexp.setPatternSyntax(QRegExp::Wildcard);
        if(!regexp.exactMatch(fileName))
            fileName.append("."+extension);

        // Setting up the QPaintDevice
        if (selectedFormat  == "PDF(*.pdf)") {
            QPdfWriter *pdfWriter = new QPdfWriter(fileName);
            pdfWriter->setPageSizeMM(sizeF);
            device.reset(pdfWriter);
        } else {
            QSvgGenerator *svgGen = new QSvgGenerator;
            svgGen->setFileName(fileName);
            svgGen->setSize(sizeF.toSize());
            svgGen->setTitle("Courbe");
            svgGen->setDescription("Courbe");
            device.reset(svgGen);
        }

        //painting on the device
        QPainter painter(device.data());
        m_lineGraph->render(&painter);
        painter.end();
    }
}

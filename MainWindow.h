#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QTableView;
class MatrixModel;
class ResultModel;
class LineGraph;
class QGraphicsView;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);

private slots:
    /**
     * @brief Recharge la matrice
     */
    void reloadMatrix();

    /**
     * @brief met a jour le graph
     */
    void updateGraph();

    /**
     * @brief Enregistre le graph sous format SVG, PNG, ou PDF
     */
    void saveGraph();

private:

    /**
     * @brief Fait les calculs
     * @return valeurs a representer dans le graph
     */
    QVector<int> calculate();

//ATTRIBUTES
private:
    QMenu *m_graphMenu, *m_matrixMenu;
    QAction *m_reloadAction, *m_saveAction;
    QTableView *m_randMatrixView, *m_trameView;
    ResultModel *m_randMatrixModel;
    MatrixModel *m_trameModel;
    QGraphicsView *m_graphicsView;
    LineGraph *m_lineGraph;
    QDockWidget *m_matrixDock, *m_trameDock;
};

#endif // MAINWINDOW_H

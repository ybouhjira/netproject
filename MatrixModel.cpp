#include "MatrixModel.h"

MatrixModel::MatrixModel(RandomMatrix const &m, QObject *parent) :
    QAbstractTableModel(parent)
  , m_matrice(m)
{
}

int MatrixModel::rowCount(const QModelIndex &) const{
    return m_matrice.getLinesCount();
}

int MatrixModel::columnCount(const QModelIndex &) const{
    return m_matrice.getColumnsCount();
}

QVariant MatrixModel::data
(const QModelIndex &index, int role = Qt::DisplayRole ) const{
    if (!index.isValid() || (role != Qt::DisplayRole && role != Qt::EditRole) )
        return QVariant();
    return m_matrice(index.row(), index.column());
}

bool MatrixModel::setData
(const QModelIndex &index, const QVariant &value, int){
    bool ok;
    double number = value.toDouble(&ok);

    // Si la conversion à été faite
    if(ok && (number==0 || number==1) ) {
        m_matrice(index.row(), index.column()) = number ;
        emit dataChanged(index,index);
        return true;
    }

    // Sinon on retourne false
    return false;
}

Qt::ItemFlags MatrixModel::flags(const QModelIndex &) const {
    return
            Qt::ItemIsEditable |
            Qt::ItemIsEnabled |
            Qt::ItemIsSelectable |
            Qt::ItemIsDragEnabled |
            Qt::ItemIsDropEnabled |
            Qt::ItemIsUserCheckable ;
}

void MatrixModel::reloadMatrix(int rowCount, int colCount) {
    beginResetModel();
    m_matrice.reload(rowCount, colCount);
    endResetModel();
}

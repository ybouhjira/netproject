#ifndef MATRIXMODEL_H
#define MATRIXMODEL_H

#include <QAbstractTableModel>
#include "RandomMatrix.h"

class MatrixModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    /**
     * @brief Constructeur
     * @param parent : object parent
     */
    MatrixModel(RandomMatrix const &m,QObject *parent = 0);

    RandomMatrix getMatrix(){
        return m_matrice;
    }

    /**
     * @brief reloadMatrix
     * @param rowCount
     * @param colCount
     */
    virtual void reloadMatrix(int rowCount, int colCount);

    // Reimplementer les methodes abstraites
    /**
     * @return le nombre de lignes
     */
    int rowCount(const QModelIndex &) const;

    /**
     * @return Le nombre de colonnes
     */
    int columnCount(const QModelIndex &) const;

    /**
     * @param index : represente l'indice d'un element dans le model
     * @return la valeur dans la case qui a l'indice index
     */
    QVariant data(const QModelIndex &index, int role) const;

    /**
     * @brief setData permet d'éditer les données à partir de la table
     * @param index : indice de la case
     * @param value : valeur
     */
    bool setData(const QModelIndex &index, const QVariant &value, int);

    /**
     * @brief Indique si l'element est editable
     * @param index : indice de l'element
     * @return true
     */
    Qt::ItemFlags flags(const QModelIndex &) const;

private:
    RandomMatrix m_matrice;
};

#endif // MATRIXMODEL_H

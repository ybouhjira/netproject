#include "RandomMatrix.h"
#include <cstdlib>

RandomMatrix::RandomMatrix(int lineCount, int columnCount)
    : m_lineCount(lineCount)
    , m_columnCount(columnCount)
{
    fill();
}

char& RandomMatrix::operator()(int line, int col) {
    return m_data[line][col];
}

char RandomMatrix::operator()(int line, int col) const {
    return m_data[line][col];
}

std::vector<char>& RandomMatrix::operator()(int l) {
    return m_data[l];
}

std::vector<char> RandomMatrix::operator()(int l)  const{
    return m_data[l];
}

void RandomMatrix::reload(int lines, int cols){
    m_lineCount = lines ;
    m_columnCount = cols ;
    m_data.clear();
    fill();
}

int RandomMatrix::getLinesCount() const {
    return m_lineCount;
}

int RandomMatrix::getColumnsCount() const {
    return m_columnCount;
}

void RandomMatrix::fill() {
    m_data = std::vector<std::vector<char> >(m_lineCount) ;
    for(int i=0; i<m_lineCount; i++)
        for(int j=0; j<m_columnCount; j++)
            m_data[i].push_back(rand()%2);
}

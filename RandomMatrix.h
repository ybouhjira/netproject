#ifndef RANDOMMATRIX_H
#define RANDOMMATRIX_H

#include <vector>

class RandomMatrix
{
// METHODES
public:
    /**
     * @brief Constructor
     */
    RandomMatrix(int lineCount, int columnCount);

    /**
     * @brief operator ()
     * @param line : La ligne
     * @param column : La column
     * @return : Reference vers l'element
     */
    char& operator()(int line, int column);


    /**
     * @brief voir la methode precedante
     */
    char operator()(int line, int column) const;

    /**
     * @brief Retourne la ligne dont l'indice est lineNumber
     */
    std::vector<char>& operator()(int lineNumber);

    /**
     * @brief Voir methode precedente
     */
    std::vector<char> operator()(int lineNumber) const;

    /**
     * @brief Genere de nouvelle valeurs pour la matrice
     * @param lines : Nombre de lignes
     * @param cols: Nombre de colonnes
     */
    void reload(int lines, int cols);

    /**
     * @brief getLinesCount
     * @return Nombre de lignes
     */
    int getLinesCount() const;

    /**
     * @brief getColumnsCount
     * @return Nombre de colonnes
     */
    int getColumnsCount() const;

private:
    void fill();

// ATTRIBUTS
private:
    std::vector<std::vector<char> > m_data;
    int m_lineCount, m_columnCount;
};

#endif // RANDOMMATRIX_H

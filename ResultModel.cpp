#include "ResultModel.h"
#include <QBrush>

ResultModel::ResultModel(RandomMatrix const &m,QObject *parent)
    : MatrixModel(m,parent)
    , m_matches(m.getLinesCount(), QVector<bool>(m.getColumnsCount(),false))
{
}

QVariant ResultModel::data(const QModelIndex &index, int role) const {
    if(index.isValid() && m_matches[index.row()][index.column()]){
        if(role == Qt::BackgroundRole)
            return QBrush(QColor(20,120,220));
        else if(role == Qt::ForegroundRole)
            return QBrush(QColor(Qt::white));
        else{
            return MatrixModel::data(index,role);
        }
    }else{
        return MatrixModel::data(index,role);
    }
}

void ResultModel::reloadMatrix(int rowCount, int colCount) {
    m_matches = QVector<QVector<bool> >(rowCount,QVector<bool>(colCount,false));
    MatrixModel::reloadMatrix(rowCount,colCount);
}

void ResultModel::setMatch(int row, int column, bool value) {
    m_matches[row][column] = value;
    /*QModelIndex index = createIndex(row,column,&m_matches[row][column]);
    qDebug(index.isValid()?"true":"false");
    QVector<int> roles;
    roles  << Qt::BackgroundRole << Qt::ForegroundRole  ;
    emit dataChanged(index, index, roles);*/
}

bool ResultModel::getMatch(int row, int column) {
    return m_matches[row][column] ;
}

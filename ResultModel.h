#ifndef RESULTMODEL_H
#define RESULTMODEL_H

#include "MatrixModel.h"

class ResultModel : public MatrixModel
{
public:
    ResultModel(RandomMatrix const &m,QObject *parent = 0);

    /**
     * @brief Reimplemented
     */
    QVariant data(const QModelIndex &index, int role) const;

    bool getMatch(int row, int column);

    void setMatch(int row, int column, bool value);

    void reloadMatrix(int rowCount, int colCount);

private:
    QVector<QVector<bool> > m_matches;
};

#endif // RESULTMODEL_H

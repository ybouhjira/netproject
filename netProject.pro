#-------------------------------------------------
#
# Project created by QtCreator 2013-01-17T20:33:41
#
#-------------------------------------------------

QT       += core gui svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = netProject
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    RandomMatrix.cpp \
    MatrixModel.cpp \
    LineGraph.cpp \
    GraphNode.cpp \
    ResultModel.cpp

HEADERS  += MainWindow.h \
    RandomMatrix.h \
    MatrixModel.h \
    LineGraph.h \
    GraphNode.h \
    ResultModel.h

RESOURCES += \
    ressources.qrc
